package br.com.mobimark.myapplication

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView

//são os itens da listagem definidos no layout/item1.xml
class Lista1ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
    val mun = itemView.findViewById<TextView>(R.id.l1_nome)
    val micro = itemView.findViewById<TextView>(R.id.l1_micro)
    val meso = itemView.findViewById<TextView>(R.id.l1_meso)
    val uf = itemView.findViewById<TextView>(R.id.l1_uf)
    val regiao = itemView.findViewById<TextView>(R.id.l1_regiao)
}

// adpater para montar a lista
class Lista1Adapter(val lista : List<Lista1>) : RecyclerView.Adapter<Lista1ViewHolder>() {
    lateinit var context : Context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Lista1ViewHolder {
        this.context = parent.context
        return Lista1ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item1, parent, false))
    }

    override fun getItemCount(): Int {
        return lista.size
    }

    //preenchimento do conteudo dos itens
    override fun onBindViewHolder(holder: Lista1ViewHolder, position: Int) {
        val item = lista[position]

        holder.mun?.text = item.mun
        holder.micro?.text = item.micro
        holder.meso?.text = item.meso
        holder.uf?.text = item.uf
        holder.regiao?.text = item.regiao


    }
}