package br.com.mobimark.myapplication

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView

class Lista2ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
    val regiao = itemView.findViewById<TextView>(R.id.l2_regiao)
    val sigla = itemView.findViewById<TextView>(R.id.l2_sigla)
}

class Lista2Adapter(val lista : List<Lista2>) : RecyclerView.Adapter<Lista2ViewHolder>() {
    lateinit var context : Context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Lista2ViewHolder {
        this.context = parent.context
        return Lista2ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item2, parent, false))
    }

    override fun getItemCount(): Int {
        return lista.size
    }

    override fun onBindViewHolder(holder: Lista2ViewHolder, position: Int) {
        val item = lista[position]

        holder.regiao?.text = item.regiao
        holder.sigla?.text = item.sigla


    }
}