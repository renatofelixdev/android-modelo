package br.com.mobimark.myapplication

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() , AdapterView.OnItemSelectedListener {

//    metodo executado ao abrir a tela
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//    carregamento do select de opções de listagem
//    as opções estão setadas no arquivo app/res/values/array
        val adapter = ArrayAdapter.createFromResource(this, R.array.exemplo_array, android.R.layout.simple_spinner_item)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = adapter
        spinner.onItemSelectedListener = this
    }

    //metodo para mudanca no select
    override fun onItemSelected(
        parent: AdapterView<*>, view: View,
        pos: Int, id: Long
    ) {
        Toast.makeText(this, parent.getItemAtPosition(pos).toString(), Toast.LENGTH_SHORT).show()

//     configuracao para fazer requisicao
        val retrofit = RetrofitInitialization.initConfig("https://servicodados.ibge.gov.br/api/v1/")
        val service = retrofit.create(RetrofitService::class.java)

        when(parent.getItemAtPosition(pos).toString()){
            "Exemplo 1" -> {
                requisicao(service.lista1(),::preencheLista1)
            }
            "Exemplo 2" -> {
                requisicao(service.lista2(),::preencheLista2)
            }
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>) {
        // Another interface callback
    }

    //metodo que faz requisicao
    fun requisicao(call: Call<JsonElement>, preencheLista: (json: JsonArray) -> Unit ){
        progress.visibility = View.VISIBLE

        call.enqueue(object : Callback<JsonElement>{
            override fun onFailure(call: Call<JsonElement>, t: Throwable) {

                Toast.makeText(this@MainActivity, "Não foi possível obter a listagem!", Toast.LENGTH_SHORT).show()
                progress.visibility = View.GONE
                lista_recycler.adapter = Lista1Adapter(listOf())
            }

            override fun onResponse(call: Call<JsonElement>, response: Response<JsonElement>) {
                if(response.isSuccessful){
                    val json = response.body()!!.asJsonArray

                    preencheLista(json)
                }else{
                    Toast.makeText(this@MainActivity, "Não foi possível obter a listagem!", Toast.LENGTH_SHORT).show()
                    lista_recycler.adapter = Lista1Adapter(listOf())
                }
                progress.visibility = View.GONE
            }

        })

    }

    //metodo que preenche a listagem apos a requisicao
    fun preencheLista1(json : JsonArray){

        //Lista1 é o modelo onde é definido as variaveis que vem na listagem
        //apenas para ficar mais prático o preenchimento da lista na tela
        val lista = arrayListOf<Lista1>()
        json.forEach {
            val obj = it.asJsonObject
            lista.add(Lista1(
                obj["nome"].asString,
                obj["microrregiao"].asJsonObject["nome"].asString,
                obj["microrregiao"].asJsonObject["mesorregiao"].asJsonObject["nome"].asString,
                obj["microrregiao"].asJsonObject["mesorregiao"].asJsonObject["UF"].asJsonObject["nome"].asString,
                obj["microrregiao"].asJsonObject["mesorregiao"].asJsonObject["UF"].asJsonObject["regiao"].asJsonObject["nome"].asString
            ))
        }

        //componente de lista no android precisa de um adapter
        lista_recycler.adapter = Lista1Adapter(lista)
    }


    fun preencheLista2(json : JsonArray){
        val lista = arrayListOf<Lista2>()
        json.forEach {
            val obj = it.asJsonObject
            lista.add(Lista2(
                obj["nome"].asString,
                obj["sigla"].asString
            ))
        }

        lista_recycler.adapter = Lista2Adapter(lista)
    }
}
