package br.com.mobimark.myapplication

import com.google.gson.JsonElement
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers


interface RetrofitService {

    //urls para requisicao

    @Headers("Content-Type: application/json")
    @GET("localidades/estados/22/municipios")
    fun lista1(): Call<JsonElement>

    @Headers("Content-Type: application/json")
    @GET("localidades/regioes")
    fun lista2(): Call<JsonElement>


}